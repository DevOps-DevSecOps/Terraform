provider "aws" {
  access_key = " "
  secret_key = " "
  region     = "ap-south-1"
}

resource "aws_instance" "Terraform_Demo" {
  ami           = "ami-0c1a7f89451184c8b"
  instance_type = "t2.micro"
  key_name = "Dev-UbuntuKey"
  tags = {
    Name = "Terraform Demo"
  }
}
