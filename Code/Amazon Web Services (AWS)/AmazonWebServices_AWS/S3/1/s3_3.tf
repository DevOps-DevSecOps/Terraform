resource "aws_s3_bucket" "BUCKET" {
  bucket = "terraform-aws-s3-buckets"
}

resource "aws_s3_bucket_object" "folder_object" {
  bucket = "${aws_s3_bucket.BUCKET.id}"
  acl    = "private"
  key    = "FoldeR/"
  source = "/var/log/syslog"
}
