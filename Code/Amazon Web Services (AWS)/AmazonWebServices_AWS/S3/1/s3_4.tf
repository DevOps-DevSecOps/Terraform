terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}


provider "aws" {}


resource "aws_s3_bucket_object" "folder-object" {
  bucket = "terraform-aws-s3-buckets"
  key    = "folders/log.txt"
  source = "/var/log/syslog.1"
}
