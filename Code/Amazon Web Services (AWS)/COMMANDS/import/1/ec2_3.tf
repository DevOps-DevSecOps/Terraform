resource "aws_instance" "instance" {
  ami = "ami-079ba66a5e9f2b70e"                                                   # existing infrastructure resource
  instance_type = "t2.micro"                                                      # existing infrastructure resource
  tags = {                                                                        # existing infrastructure resource
    Name = "Suse"
  }
  count = 2
}
