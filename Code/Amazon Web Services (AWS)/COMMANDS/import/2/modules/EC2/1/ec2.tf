resource "aws_instance" "instance" {
  ami = "ami-00aa0673b34e3c150"                                                   # existing infrastructure resource
  instance_type = "t2.micro"                                                      # existing infrastructure resource
  tags = {                                                                        # existing infrastructure resource
    Name = "RedHat"
  }
}
