provider "aws" {
    region = "us-west-2"
    profile = "terraform-user"
}

resource "aws_instance" "demo-instance" {
  ami           = "ami-095413544ce52437d"
  instance_type = "t2.micro"

  tags = {
      "Name": "demo-ec2-instance"
  }
}
