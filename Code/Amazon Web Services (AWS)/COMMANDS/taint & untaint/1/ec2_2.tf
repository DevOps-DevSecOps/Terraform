provider "aws" {
  region = "us-east-1"
}

variable "ami" {
  default = "ami-05cc83e573412838f"
}

variable "instance_type" {
  type = string
  default = "t3.medium"
}

resource "aws_instance" "my_vm_1" {
  ami           = var.ami //Ubuntu AMI
  instance_type = var.instance_type
 
  tags = {
    Name = "VM 1",
  }
}
 
resource "aws_instance" "my_vm_2" {
  ami           = var.ami //Ubuntu AMI
  instance_type = var.instance_type
 
  tags = {
    Name = "VM 2",
  }
}

output "my_vm_1_id" {
  value = aws_instance.my_vm_1.id
}

output "my_vm_1_private-ip" {
  value = aws_instance.my_vm_1.private_ip
}

output "my_vm_1_public-ip" {
  value = aws_instance.my_vm_1.public_ip
  sensitive   = true
}

output "my_vm_2_id" {
  value = aws_instance.my_vm_2.id
}

output "my_vm_2_private-ip" {
  value = aws_instance.my_vm_2.private_ip
}

output "my_vm_2_public-ip" {
  value = aws_instance.my_vm_2.public_ip
  sensitive   = true
}
