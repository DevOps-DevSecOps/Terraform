# This is a single line comment

provider "aws" {
  region = "us-east-1"
}

// This is also a single line comment

resource "aws_instance" "example_instance" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t2.micro"
}

/*
This is an
Multiple Lines
comment
*/
