provider "aws" {
  profile = "default"
  region = "us-east-1"
}

resource "aws_instance" "example_instance" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t2.micro"
}
