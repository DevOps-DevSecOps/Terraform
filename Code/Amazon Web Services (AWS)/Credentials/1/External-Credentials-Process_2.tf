provider "aws" {
  profile = "customprofile"
  region = "us-west-1"
}

resource "aws_instance" "example_instance" {
  ami           = "ami-033220e23cb64e5be"
  instance_type = "t2.micro"
}
