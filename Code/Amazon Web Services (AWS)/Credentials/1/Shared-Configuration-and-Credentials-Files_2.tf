# custom path for credential files with other different profile name in AWS.

provider "aws" {
  shared_config_files      = ["/home/ubuntu/.aws/conf"]
  shared_credentials_files = ["/home/ubuntu/.aws/creds"]
  profile                  = "customprofile"
  region                   = "us-east-1"
}

resource "aws_instance" "example_instance" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t2.micro"
}
