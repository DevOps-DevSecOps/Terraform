provider "aws" {
  region = "us-east-1"
}

data "aws_s3_bucket" "example1" {
  bucket = "example-bucket"
}

data "aws_s3_bucket" "example2" {
  region = "us-east-1"
}

output "bucket_name" {
  value = data.aws_s3_bucket.example1.bucket
}

output "bucket_region" {
  value = data.aws_s3_bucket.example2.region
}
