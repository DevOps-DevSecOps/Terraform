# Provider
provider "aws" {
  profile = "aws"
  region = "us-east-2"
}

data "aws_subnet" "subnet" {
  id = "subnet-09c7c6025345c3d12"
}

# AWS Network_Interface
resource "aws_network_interface" "test" {
  subnet_id       = data.aws_subnet.subnet.id
  private_ips     = ["10.0.1.1"]
}

# AWS EC2
resource "aws_instance" "foo" {
  ami           = "ami-005e54dee72cc1d00" 
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.test.id
    device_index         = 0
  }

  tags = {
    Name = "ec2_demo"
  }
}
