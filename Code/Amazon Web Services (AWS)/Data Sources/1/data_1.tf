provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20230325"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_availability_zones" "available" {
  state = "available"
}

output "virtual_machine_id" {
  value = data.aws_availability_zones.available.names[0]
}

output "virtual_machine_id1" {
  value = data.aws_availability_zones.available.names[1]
}


output "ips_with_list_interpolation" {
  value = [ for name in data.aws_availability_zones.available.names : name ]
}

resource "aws_instance" "first-ec2" {
  ami           = data.aws_ami.ubuntu.id # us-west-2
  instance_type = "t2.micro"
  tags = {
    Name = "RajeshKumar"
  }
}
