data "aws_ami" "amazon-linux2"{
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm*"]
  }
}

data "aws_availability_zones" "az" {
  state = "available"
}

output "ami_ID" {
  value = data.aws_ami.amazon-linux2.id
}

output "az" {
  value = data.aws_availability_zones.az.names
}
