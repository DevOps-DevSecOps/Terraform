provider "aws" {
  region  = "us-west-2"
}

resource "aws_instance" "app_server" {
  instance_type = "t2.micro"
  ami           = "ami-830c94e3"
  tags = {
    Name = "ExampleAppServerInstance"
  }
}
