provider "aws" {
  region     = "us-west-2"
}

resource "aws_instance" "web1" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t3.micro"

  tags = {
    Name = "HelloWorld-1"
  }
}

resource "aws_instance" "web2" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t3.micro"

  tags = {
    Name = "HelloWorld-2"
  }
}
