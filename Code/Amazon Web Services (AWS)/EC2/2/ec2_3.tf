terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

provider "aws" {
  access_key = "ACCESS_KEY"
  secret_key = "SECRET_KEY"
  region     = "us-west-2"
}

resource "aws_instance" "web1" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t3.micro"
}

resource "aws_instance" "web2" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t3.micro"
}
