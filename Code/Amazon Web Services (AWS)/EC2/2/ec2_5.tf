provider "aws" {
  region  = "us-east-1"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}

resource "aws_instance" "example_instance" {
  ami           = "ami-005e54dee72cc1d00"
  instance_type = "t2.micro"
}
