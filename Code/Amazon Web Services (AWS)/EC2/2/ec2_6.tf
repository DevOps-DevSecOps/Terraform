provider "aws" {
  region     = "eu-central-1"
  shared_credentials_files = ["/Users/rahulwagh/.aws/credentials"]
}


resource "aws_instance" "example" {
  ami = "ami-0767046d1677be5a0"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["sg-0cb45dab1790b3e7c"]
  key_name= "aws_key"
  user_data     =  "${file("install_apache.sh")}"
}
