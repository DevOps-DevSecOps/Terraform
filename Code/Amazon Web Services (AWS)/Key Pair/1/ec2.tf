resource "aws_instance" "app_server" {
  ami           = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  key_name = aws_key_pair.key.key_name
  tags = {
    name = "EC2"
  }
}
