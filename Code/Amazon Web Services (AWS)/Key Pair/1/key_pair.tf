resource "aws_key_pair" "key" {
  key_name   = "ssh"
  public_key = file("aws.pub")
  tags = {
    Name = "AWS-Key"
  }
}

