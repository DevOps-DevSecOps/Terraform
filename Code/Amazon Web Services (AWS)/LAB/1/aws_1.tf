# Provider
provider "aws" {
  profile = "aws"
  region = "us-east-1"
}

# AWS VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
}

# AWS Subnet
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main"
  }
}

# AWS Network_Interface
resource "aws_network_interface" "test" {
  subnet_id       = aws_subnet.main.id
  private_ips     = ["10.0.1.1"]
}

# AWS EC2
resource "aws_instance" "foo" {
  ami           = "ami-005e54dee72cc1d00" 
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.test.id
    device_index         = 0
  }
}
