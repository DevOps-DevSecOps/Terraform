terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

# provider
provider "aws" {
  region = "us-west-2"
}

# VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  tags = {
    Name = "VPC"
  }
}

# Subnet_Public
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = "Subnet_Public"
  }
}

# Subnet_Private
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-west-2c"
  tags = {
    Name = "Subnet_Private"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "Internet Gateway"
  }
}

# Route Table 
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
# route {
#   cidr_block = aws_vpc.main.cidr_block
#   gateway_id = aws_internet_gateway.gw.id
# }
  tags = {
    Name = "RouteTable_Public"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
# route {
#   cidr_block = "10.0.0.0/16"
#   gateway_id = aws_internet_gateway.gw.id
# }
  tags = {
    Name = "RouteTable_Private"
  }
}

# Route Table Associations
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

# Route
resource "aws_route" "public" {
  route_table_id            = aws_route_table.public.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = aws_internet_gateway.gw.id
  depends_on                = [aws_route_table.public]
}

# Security Group
resource "aws_security_group" "public" {
  name        = "public"
  description = "Allow traffic for webserver"
  vpc_id      = aws_vpc.main.id
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "public"
  }
}

resource "aws_security_group" "private" {
  name        = "private"
  description = "Allow traffic for webserver"
  vpc_id      = aws_vpc.main.id
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "private"
  }
}

# Elastic Network Interface
resource "aws_network_interface" "public" {
  subnet_id       = aws_subnet.public.id
  private_ips     = ["10.0.1.7"]
  security_groups = [aws_security_group.public.id]
}

resource "aws_network_interface" "private" {
  subnet_id       = aws_subnet.private.id
  private_ips     = ["10.0.2.7"]
  security_groups = [aws_security_group.private.id]
}

# EC2
resource "aws_instance" "public" {
  ami           = "ami-005e54dee72cc1d00" 
  instance_type = "t2.micro"
# vpc_security_group_ids = ["aws_security_group.public.id"]
  network_interface {
    network_interface_id = aws_network_interface.public.id
    device_index         = 0
  }
  tags = {
    Name = "public"
  }
}

resource "aws_instance" "private" {
  ami           = "ami-0638cf52a5b22be19" 
  instance_type = "t2.micro"
# vpc_security_group_ids = ["aws_security_group.private.id"]
  network_interface {
    network_interface_id = aws_network_interface.private.id
    device_index         = 0
  }
  tags = {
    Name = "private"
  }
}

