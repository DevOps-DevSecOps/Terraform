provider "aws" {
  region  = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

data "aws_availability_zones" "available" { }

resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "Default subnet"
  }
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow 8080 & 22 inbound traffic"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    description      = "SSH access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "jenkins access"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_instance" "jenkins_server" {
  ami           = "ami-005e54dee72cc1d00" # us-west-2
  instance_type = "t2.micro"
  subnet_id     = aws_default_subnet.default_az1.id
  vpc_security_group_ids = [ aws_security_group.allow_tls.id ]
  key_name = "aws"

  tags = {
    "Name" = "jenkins"
  }
}


resource "null_resource" "name" {
  # SSH into EC2 Instance
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("~/FOLDER_NAME/aws.pem")
    host = aws_instance.jenkins_server.public_ip
  }
  # copy the jenkins.sh file from local to ec2 instance
  provisioner "file"{
    source = "jenkins.sh"
    destination = "/opt/jenkins.sh"
  }
  # set permission and execute the script file
  provisioner "remote-exec"{
    inline = [
      "sudo chmod +x /opt/jenkins.sh",
      "sh /opt/jenkins.sh"
    ]
  }  
  depends_on = [
    aws_instance.jenkins_server
  ]
}
