provider "aws" {
  profile = "aws"
  region = "us-east-1"
}

provider "aws" {
  alias  = "east-2"
  profile = "devops"
  region = "us-east-2"
}
