module "VPC" {
  source   = "./Modules/VPC"
  vpc_cidr = var.vpc_cidr
  vpc_tag  = var.vpc_tag
}

module "Subnet" {
  source      = "./Modules/Subnet"
  vpc_id      = module.VPC.vpc_id
  subnet_cidr = var.subnet_cidr
  subnet_name = var.subnet_name
}

module "SecurityGroup" {
  source  = "./Modules/SecurityGroup"
  vpc_id  = module.VPC.vpc_id
  sg_name = var.sg_name
}

module "NetworkInterface" {
  source      = "./Modules/NetworkInterface"
  for_each    = var.instances
  subnet_id   = module.Subnet.subnet_id
  nic_name    = each.value.nic_name
  private_ips = each.value.private_ips
}

module "EC2" {
  source        = "./Modules/EC2"
  for_each      = var.instances
  instance_Name = each.value.instance_Name
  instance_ami  = each.value.instance_ami
  instance_type = each.value.instance_type
  nic_id        = module.NetworkInterface[each.key].nic_id
}
