vpc_cidr = "172.16.0.0/16"

vpc_tag = {
  "Name" = "vpc"
}

subnet_cidr = "172.16.10.0/24"

subnet_name = {
  "Name" = "subnet"
}

sg_name = {
  "Name" = "allow_tls"
}

nic_name = {
  "Name" = "my_nic"
}

private_ips = ["172.16.10.100"]

instance_Name = {
  "Name" = "PROD"
}

instance_ami = "ami-005e54dee72cc1d00"

instance_type = "t2.micro"
