vpc_cidr = "172.16.0.0/16"

vpc_tag = {
  "Name" = "AWS_vpc"
}

subnet_cidr = "172.16.10.0/24"

subnet_name = {
  "Name" = "AWS_subnet"
}

sg_name = {
  "Name" = "allow_tls"
}

nic_name = {
  "Name" = "AWS_nic"
}

private_ips = ["172.16.10.100"]

instance_Name = {
  "Name" = "AWS_EC2"
}

instance_ami = "ami-005e54dee72cc1d00"

instance_type = "t2.micro"
