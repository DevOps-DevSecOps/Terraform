terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.16.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
}

resource "aws_instance" "web1" {
  ami             = "ami-080e1f13689e07408"
  instance_type   = "t2.micro"
  key_name        = "n.virginia"
  subnet_id       = "subnet-04165a78352973e31"
  vpc_security_group_ids = [aws_security_group.SG.id]
  tags = {
    Name = "HelloWorld-1"
  }
  root_block_device {
    volume_type = "gp2"
    volume_size = 18
  }
}

resource "aws_instance" "web2" {
  ami             = "ami-080e1f13689e07408"
  instance_type   = "t2.micro"
  key_name        = "n.virginia"
  subnet_id       = aws_subnet.SUBNET.id
  vpc_security_group_ids = [aws_security_group.SG.id]
  tags = {
    Name = "HelloWorld-2"
  }
  root_block_device {
    volume_size = 15
  }
  user_data = templatefile("./install.sh", {})
}

resource "aws_default_vpc" "VPC" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_subnet" "SUBNET" {
  vpc_id     = aws_default_vpc.VPC.id
  cidr_block = "172.31.96.0/20"
  availability_zone = "us-east-1a"
  tags = {
    Name = "Subnet"
  }
}

resource "aws_route_table_association" "RT" {
  subnet_id = aws_subnet.SUBNET.id
  route_table_id = "rtb-0fa3da68f6545d06e"
}

resource "aws_security_group" "SG" {
  name        = "allow_tls"
  description = "Allow 8080 & 22 inbound traffic"
  vpc_id      = aws_default_vpc.VPC.id

  ingress = [
    for port in [22, 80, 443, 8080, 9000] : {
      description = "inbound rules"
      from_port = port
      to_port = port
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    } 
  ]
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

