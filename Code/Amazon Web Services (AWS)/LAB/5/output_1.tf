output "vpc_id" {
  description = "output values of VPC"
  value = aws_vpc.main.id
  sensitive   = true
}

output "vpc_tags" {
  value = aws_vpc.main.tags_all
}

output "vpc_ownerID" {
  value = aws_vpc.main.owner_id
}

output "public_id" {
  description = "ID of the EC2 instance Public"
  value = aws_instance.public.id
}

output "public_tags" {
  value = aws_instance.public.tags
}

output "ami_region" {
  value = lookup(var.aws_amis, var.aws_region)
}

output "bastion_host-ids" {
  description = "IDs of EC2 instances"
  value       = aws_instance.bastion_host.*.id
}

output "no_of_instances" {
  value = length(aws_instance.bastion_host.*.id)
}

