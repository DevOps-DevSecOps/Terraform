variable "name" {
  type = string
  default = "private"
}

variable "aws_amis" {
  type = map
  default = {
    "us-east-1" = "ami-053b0d53c279acc90"
    "us-west-2" = "ami-0638cf52a5b22be19"
    "us-east-2" = "ami-024e6efaf93d85776"
  }
}

variable "aws_region" {
  description = "AWS region"
  default = "us-west-2"
}

variable "tags" {
  type = map(string)
}

variable "count_no" {
  type = number
}

