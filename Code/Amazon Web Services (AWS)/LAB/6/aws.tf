provider "aws" {
  region = "us-east-1"
  profile = "devops"
}


terraform {
  backend "s3" {
    bucket = "terraformS3"
    key    = "terraform.tfstate"
    region = "us-east-1"
    encrypt = true
    profile = "devops"
  }
}


resource "aws_s3_bucket" "bucket" {
  bucket = "my-tf-test-bucket"
  acl    = "private"
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

output "S3_Bucket" {
  value = aws_s3_bucket.bucket.id
}


resource "aws_instance" "ec2_example" {
  ami = "ami-00831fc7c1e3ddc60"
  instance_type = "t2.micro"
  count = 1
  tags = {
    Name = var.environmentName
  }
}

output "ec2_output" {
  value = "My Console OUTPUT"
}
