variable "environmentName"{
  description = "Name of the environment"
  type        = string
  default     = "TerraformEC2"
}
