resource "aws_instance" "web_1" {
# ami = "ami-0638cf52a5b22be19"
  ami = "ami-0744bdf45532dfd8e"
  instance_type = "t2.micro"
  tags = {
    "Name" = "replace_triggered_by-1"
  }
}

resource "aws_instance" "web_2" {
  ami = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  tags = {
    "Name" = "replace_triggered_by-2"
  }
  lifecycle {
    replace_triggered_by = [
      aws_instance.web_1.id,
    ]
  }
}
