resource "aws_instance" "web_1" {
  ami = "ami-0638cf52a5b22be19"
# ami = "ami-0744bdf45532dfd8e"
  instance_type = "t2.micro"
  tags = {
    "Name" = "replace_triggered_by-1"
  }
}

resource "aws_instance" "web_2" {
  ami = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
# vpc_security_group_ids = ["aws_security_group.web_server.id"]
  tags = {
    "Name" = "replace_triggered_by-2"
  }
  lifecycle {
    replace_triggered_by = [
      aws_instance.web_1.id,
    ]
  }
}


resource "aws_security_group" "web_server" {
  name        = "web_server"
  description = "Allow traffic for webserver"
  vpc_id      = "vpc-0ad84d2db9d3d8bbe"
 
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
 
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
 
  tags = {
    Name = "web_server_2"
  }

  lifecycle {
    replace_triggered_by = [ aws_instance.web_2.id ]
  }

}

