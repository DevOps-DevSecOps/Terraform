# Create EC2 Instance
resource "aws_instance" "instance" {
  ami               = "ami-0127b2e6f3b9b94d5" # Amazon Linux 2
  instance_type     = "t3.micro"
  availability_zone = "us-west-1b"
  tags = {
    "Name" = "demo2"
  }
  lifecycle {
    prevent_destroy = True # Default is false
  }
}
