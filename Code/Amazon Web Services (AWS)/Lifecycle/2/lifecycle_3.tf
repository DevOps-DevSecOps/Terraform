resource "aws_instance" "instance" {
  ami           = "ami-0127b2e6f3b9b94d5" # Amazon Linux 2
  instance_type = "t2.micro"
  availability_zone = "us-west-1b"

  tags = {
    Name = "demo4"
  }

  lifecycle {
    ignore_changes = [
      user_data,
      tags.Name,
    ]
  }
}
