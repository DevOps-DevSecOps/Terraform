provider "aws" {
  region = "us-west-2"
}

resource "aws_vpc" "main" {
# cidr_block       = "10.0.0.0/16"
  cidr_block       = "172.16.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default"

  lifecycle {
    ignore_changes = [
      cidr_block    
    ]
  }
}

