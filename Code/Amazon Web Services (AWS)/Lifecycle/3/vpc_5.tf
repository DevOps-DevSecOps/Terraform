resource "aws_vpc" "main" {
# cidr_block       = "10.0.0.0/16"
  cidr_block       = "10.0.0.0/17"

  tags = {
    Name = "VPC"
  }
}

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-west-2c"

  tags = {
    Name = "Subnet"
  }

  lifecycle {
    replace_triggered_by = [
      aws_vpc.main.id
    ]
  }
}

