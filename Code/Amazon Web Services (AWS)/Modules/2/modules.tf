module "Instance" {
  source = "./modules/EC2"
  vpc_id = module.Network.vpc
  subnet_public_id = module.Network.subnet_public
  subnet_private_id = module.Network.subnet_private
}

module "Network" {
  source = "./modules/Network"
}

