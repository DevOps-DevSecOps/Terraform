output "vpc" {
  value = aws_vpc.main.id
}

output "subnet_public" {
  value = aws_subnet.public.id
}

output "subnet_private" {
  value = aws_subnet.private.id
}

output "route_table_public" {
  value = aws_route_table.public.id
}

output "route_table_private" {
  value = aws_route_table.private.id
}

output "route_table_association_public" {
  value = aws_route_table_association.public.id
}

output "route_table_association_private" {
  value = aws_route_table_association.private.id
}

output "route_public" {
  value = aws_route.public.id
}

output "internet_gateway" {
  value = aws_internet_gateway.gw.id
}

