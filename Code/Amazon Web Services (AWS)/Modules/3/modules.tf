module "Instance" {
  source = "./modules/EC2"

  vpc_id = module.Network.vpc
  subnet_public_id = module.Network.subnet_public
  subnet_private_id = module.Network.subnet_private
}


module "Network" {
  source = "./modules/Network"
}


module "S3" {
  source = "./modules/S3"

  bucket_name = "robin-example-2023-08-22"
  tag_s3_bucket = {
    Name = "creating a S3 bucket"
    environment = "PRODUCTION"
  }
}

