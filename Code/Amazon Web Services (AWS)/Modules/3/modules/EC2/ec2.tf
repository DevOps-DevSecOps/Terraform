resource "aws_instance" "public" {
  ami           = "ami-005e54dee72cc1d00" 
  instance_type = "t2.micro"
  network_interface {
    network_interface_id = aws_network_interface.public.id
    device_index         = 0
  }
  tags = {
    Name = "public"
  }
}

resource "aws_instance" "private" {
  ami           = "ami-0638cf52a5b22be19" 
  instance_type = "t2.micro"
  network_interface {
    network_interface_id = aws_network_interface.private.id
    device_index         = 0
  }
  tags = {
    Name = "private"
  }
}


