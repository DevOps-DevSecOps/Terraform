output "security_group_public" {
  value = {
    id = aws_security_group.public.id
    tag = aws_security_group.public.tags_all
  }
}

output "security_group_private" {
  value = {
    id = aws_security_group.private.id
    tag = aws_security_group.private.tags_all
  }
}

output "instance_public" {
  value = {
    id = aws_instance.public.id
    instance_type = aws_instance.public.instance_type
    tag = aws_instance.public.tags
    volume_id = aws_instance.public.root_block_device[0].volume_id
  }
}

output "instance_private" {
  value = {
    id = aws_instance.private.id
    instance_type = aws_instance.private.instance_type
    tag = aws_instance.private.tags
    volume_id = aws_instance.private.root_block_device.0.volume_id
  }
}

