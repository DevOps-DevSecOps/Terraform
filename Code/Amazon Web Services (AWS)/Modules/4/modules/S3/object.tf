resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.s3_bucket.id
  content_type = "html"
  key    = "VM"
  source = "/home/azureuser/VM.pem"

  tags = {
    upload = var.upload_s3_object
    uploading = "files"
  }
}

