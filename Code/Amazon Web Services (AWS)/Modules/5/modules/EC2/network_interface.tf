resource "aws_network_interface" "public" {
  count = var.public_ni_count
  subnet_id       = var.subnet_public_id
  private_ip      = "${element(["10.0.1.22", "10.0.1.21"], count.index)}"
# private_ips = "${element(["10.0.1.33", "10.0.1.11"], count.index)}"
# private_ips_count = 2
  security_groups = [aws_security_group.public.id]
  tags = {
    Name = "public_${count.index}"
  }
  attachment {
    instance = "${element(aws_instance.public.*.id, count.index)}"
    device_index = "${count.index + 1}"
  }
}

resource "aws_network_interface" "private" {
  subnet_id       = var.subnet_private_id
  private_ips     = ["10.0.2.7"]
  security_groups = [aws_security_group.private.id]
  tags = {
    Name = "private"
  }
}


/*
resource "aws_network_interface_attachment" "public" {
  count = 2
  instance_id = "${element(aws_instance.public.*.id, count.index)}"
  network_interface_id = "${element(aws_network_interface.public.*.id, count.index)}"
  device_index         = "${count.index + 1}"
}
*/

