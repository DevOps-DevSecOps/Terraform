output "security_group_public" {
  value = {
    id = aws_security_group.public.id
    tag = aws_security_group.public.tags_all
  }
}

output "security_group_private" {
  value = {
    id = aws_security_group.private.id
    tag = aws_security_group.private.tags_all
  }
}

output "network_interface_public_id" {
  value = aws_network_interface.public.*.id
}

output "network_interface_public_MAC" {
  value = aws_network_interface.public.*.mac_address
}

output "network_interface_public_count" {
  value = length(aws_network_interface.public.*.id)
}

output "network_interface_private_id" {
  value = aws_network_interface.private.*.id
}

output "network_interface_private_MAC" {
  value = aws_network_interface.private.*.mac_address
}

output "network_interface_private_count" {
  value = length(aws_network_interface.private.*.id)
}

output "instance_public" {
  value = {
    id = aws_instance.public.*.id
    instance_type = aws_instance.public.*.instance_type
    tag = aws_instance.public.*.tags
    volume_id = aws_instance.public.*.root_block_device[1].*.volume_id
    public_ip = aws_instance.public.*.public_ip
    private_ip = aws_instance.public.*.private_ip
   }
}

output "instance_public_count" {
  value = length(aws_instance.public.*.id)
}

output "instance_private" {
  value = {
    id = aws_instance.private.*.id
    instance_type = aws_instance.private.*.instance_type
    tag = aws_instance.private.*.tags
    volume_id = aws_instance.private.*.root_block_device.0.volume_id
    public_ip = aws_instance.private.*.public_ip
    private_ip = aws_instance.private.*.private_ip
    count = length(aws_instance.private.*.id)
  }
}

