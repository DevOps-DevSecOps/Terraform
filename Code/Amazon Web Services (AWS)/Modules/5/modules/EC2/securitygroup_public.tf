resource "aws_security_group" "public" {
  name        = "public"
  description = "Allow traffic for webserver"
  vpc_id      = var.vpc_id
  ingress {
    description      = "Allow all traffic from everywhere"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "public"
  }
}

