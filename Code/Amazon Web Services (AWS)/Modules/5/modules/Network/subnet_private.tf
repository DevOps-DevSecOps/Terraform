resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
# cidr_block = "10.0.2.0/24"
  cidr_block = var.cidr_private
# availability_zone = "us-west-2c"
  availability_zone = var.az_private
  tags = {
    Name = "Subnet_Private"
  }
}

