variable "vpc_tag" {
  default     = {
    project     = "project-Delta",
    environment = "PROD"
  }
}

variable "az_public" {
  description = "az for public subnet"
  type = string
}

variable "cidr_private" {
  type = string
}

variable "az_private" { }

variable "destination_cidr_public" {
}

variable "ig_tag" {}

