output "region_s3_bucket" {
  value = aws_s3_bucket.s3_bucket.region
}

output "bucket_s3_bucket" {
  value = aws_s3_bucket.s3_bucket.id
}

output "tags_s3_bucket" {
  value = aws_s3_bucket.s3_bucket.tags
}


output "s3_object" {
  description = "detailed information of s3_object"
  value = {
    id = aws_s3_object.object.id
    version_id = aws_s3_object.object.version_id
    tag = aws_s3_object.object.tags
  }
}


output "s3_bucket_versioning" {
  value = aws_s3_bucket_versioning.versioning_example.id
}


output "s3_bucket_website" {
  value = {
    id = aws_s3_bucket_website_configuration.s3_bucket.id
    website_endpoint = aws_s3_bucket_website_configuration.s3_bucket.website_endpoint
  }
}

