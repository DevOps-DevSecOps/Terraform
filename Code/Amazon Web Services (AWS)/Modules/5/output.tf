output "vpc" {
  description = "ID of project VPC"
  value       = module.Network.vpc
}

output "subnet_public" {
  value = module.Network.subnet_public
}

output "subnet_private" {
  value = module.Network.subnet_private
}

output "route_table_public" {
  value = module.Network.route_table_public
}

output "route_table_private" {
  value = module.Network.route_table_private
}

output "route_table_association_public" {
  value = module.Network.route_table_association_public
}

output "route_table_association_private" {
  value = module.Network.route_table_association_private
}

output "route_public" {
  value = module.Network.route_public
}

output "internet_gateway" {
  value = module.Network.internet_gateway
}

output "security_group_public" {
  value = module.Instance.security_group_public.id
}

output "security_group_private" {
  value = module.Instance.security_group_private.tag
}

output "instance_public_with_count" {
  value = {
    details = module.Instance.instance_public
    count = module.Instance.instance_public_count
  }
}

output "instance_private" {
  value = {
    id = module.Instance.instance_private.id
    instance_type = module.Instance.instance_private.instance_type
    tag = module.Instance.instance_private.tag
    volume_id = module.Instance.instance_private.volume_id
    public_ip = module.Instance.instance_private.public_ip
    private_ip = module.Instance.instance_private.private_ip
  }
}

output "instance_private_count" {
  value = module.Instance.instance_private.count
}

output "network_interface_public_id" {
  value = module.Instance.network_interface_public_id
}

output "network_interface_public_MAC" {
  value = module.Instance.network_interface_public_MAC
}

output "network_interface_public_count" {
  value = module.Instance.network_interface_public_count
}

output "network_interface_private_id" {
  value = module.Instance.network_interface_private_id
}

output "network_interface_private_MAC" {
  value = module.Instance.network_interface_private_MAC
}

output "network_interface_private_count" {
  value = module.Instance.network_interface_private_count
}

output "s3" {
  value = {
    region_s3_bucket = module.S3.region_s3_bucket
    bucket_s3_bucket = module.S3.bucket_s3_bucket
    tags_s3_bucket = module.S3.tags_s3_bucket
    s3_object = module.S3.s3_object
    s3_bucket_versioning = module.S3.s3_bucket_versioning
    s3_bucket_website = module.S3.s3_bucket_website.id
  }
}

