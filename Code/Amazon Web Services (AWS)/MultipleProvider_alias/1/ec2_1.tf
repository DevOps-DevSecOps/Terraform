resource "aws_instance" "west-1" {
  provider      = aws.aws_west_1
  ami           = var.ami_west_1
  instance_type = var.type

  tags = {
    name = "Demo System"
  }
}
