output "west_1_instance_id" {
  value = aws_instance.west-1[*].id
}

output "west_2_instance_id" {
  value = aws_instance.west-2[*].id
}
