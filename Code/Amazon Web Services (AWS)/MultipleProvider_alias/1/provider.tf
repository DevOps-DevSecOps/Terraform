provider "aws" {
  alias  = "aws_west_1"
  region = var.region_west_1
}

provider "aws" {
  alias  = "aws_west_2"
  region = var.region_west_2
}
