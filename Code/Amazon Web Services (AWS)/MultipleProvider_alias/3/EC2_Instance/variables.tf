variable "region" {
  description = "AWS region"
}

variable "server_name" {
  description = "Name tag for the web server"
}

variable "ami" {
  description = "AMI for the web server"
}

variable "instance_type" {
  description = "Instance Type for the web server"
}

variable "key_name" {
  description = "Key for the web server"
}
