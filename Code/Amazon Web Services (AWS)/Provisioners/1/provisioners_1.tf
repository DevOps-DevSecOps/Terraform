provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "resource1" {
  instance_type = "t2.micro"
  ami           = "ami-0638cf52a5b22be19"
  timeouts {                          # Customize your operations longevity
   create = "60m"
   delete = "2h"
   }
}
 
resource "aws_instance" "resource2" {
  instance_type = "t2.micro"
  ami           = "ami-0638cf52a5b22be19"
  key_name      = "EC2"

  provisioner "local-exec" {
    command = "echo 'Automateinfra.com' >text.txt"
  }

  provisioner "file" {
    source      = "text.txt"
    destination = "/tmp/text.txt"

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.resource2.public_ip}"
      port = 22
    }
  }
  
  provisioner "remote-exec" {
    inline = [
      "sudo apt install apache2",
    ]

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.resource2.public_ip}"
      port = 22
    }
  }

}

