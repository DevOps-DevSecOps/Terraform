resource "aws_instance" "web" {
  ami           = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"

  provisioner "local-exec" {
    command = "echo The servers IP address is ${self.private_ip}"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "echo 'Destroy-time provisioner'"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "sudo rm -rf *"
  }

}
