provider "aws" {
  region  = "us-west-2"
}


resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name      = "EC2"

  provisioner "local-exec" {
#   command = "echo ${aws_instance.app_server.private_ip} >> private_ips.txt"
    command = "echo ${aws_instance.app_server.public_ip} >> public_ips.txt"
  }
}

