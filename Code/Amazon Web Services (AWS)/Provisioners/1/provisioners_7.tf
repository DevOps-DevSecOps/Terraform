resource "aws_instance" "app_server" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
  key_name      = "EC2"

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo hostname"
    ]

    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("/home/azureuser/EC2.pem")}"
      host = "${aws_instance.app_server.public_ip}"
      port = 22
    }
  }
}

