resource "aws_instance" "web" {
  ami             = "ami-080e1f13689e07408"
  instance_type   = "t2.micro"
  key_name        = "n.virginia"
  vpc_security_group_ids = [aws_security_group.demo_sg.id]

  tags = {
    Name = "HelloWorld"
  }

  user_data = file("script.sh")

}
