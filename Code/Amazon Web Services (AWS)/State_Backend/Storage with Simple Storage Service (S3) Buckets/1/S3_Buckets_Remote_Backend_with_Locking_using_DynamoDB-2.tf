################### PROVIDER ##############################
variable "access_key" {
  type = string
}
variable "secret_key" {
  type = string
}
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = "us-east-1"
}
################### S3 as BACKEND ##############################
terraform {
  backend "s3" {
        # Replace this with your bucket name!
    bucket         = "terraform-state-test-bucket-state-file"
    key            = "workspace-test/terraform.tfstate"
    region         = "us-east-1"
        # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-lock"
  }
}
################### EC2 INSTANCE ##############################
resource "aws_instance" "test" {
  ami                         = "ami-052efd3df9dad4825"
  instance_type               = "t2.micro" 
}
