provider "aws" {
  shared_credentials_files = ["~/.aws/credentials"]
  region     = "us-west-1"
}

resource "aws_s3_bucket" "tf_course" {  
    bucket = "hella-buckets"
}
