Backend block in Terraform does not allow you to use any variables or references. The following code will not work.

# This will NOT work. Variables aren't allowed in a backend configuration.
terraform {
  backend "s3" {
    bucket         = var.bucket
    region         = var.region
    dynamodb_table = var.dynamodb_table
    key            = "example/terraform.tfstate"
    encrypt        = true
  }
}

This means that you need to manually copy and paste the S3 bucket name, region, DynamoDB table name, etc., into every one of your Terraform modules.

One option for reducing copy-and-paste is to use partial configurations, where you omit certain parameters from the backend configuration in Terraform code and instead pass those in via -backend-config command-line arguments when calling terraform init.

For example, you could extract the repeated backend arguments, such as bucket and region, into a separate file called "backend.hcl".

# backend.hcl
bucket         = "terraform-up-and-running-state"
region         = "us-east-2"
dynamodb_table = "terraform-up-and-running-locks"
encrypt        = true

Only the 'key' parameter remains in the Terraform code, since you still need to set a different 'key' value for each module.

# Partial configuration. The other settings (e.g., bucket, region) 
# will be passed in from a file via -backend-config arguments to 
# 'terraform init'
terraform {
  backend "s3" {
    key = "example/terraform.tfstate"
  }
}

So run the command terraform init with the -backend-config argument.

$ terraform init -backend-config=backend.hcl



# LINK :- https://blog.gruntwork.io/how-to-manage-terraform-state-28f5697e68fa
