provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "main" {
  cidr_block = "10.20.0.0/16"
  instance_tenancy = "default"
  tags = {
    "Name" = "Java Home"
    "Location" = "Banglore"
    "Department" = "Training"
  }
}

resource "aws_subnet" "subnet1" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.20.0.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "Subnet1"
    Location = "India"
  }
}

terraform {
  backend "s3" {
    bucket = "jhc-iac-state"
    region = "us-east-1"
    key = "terraform.tfstate"
    dynamodb_table = "state-lock"
  }
}
