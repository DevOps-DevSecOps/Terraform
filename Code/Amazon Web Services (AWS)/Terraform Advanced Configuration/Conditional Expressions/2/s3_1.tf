resource "aws_s3_bucket" "assets" {
  count  = "${terraform.workspace == "production" ? 1 : 0}"
  bucket = "assets.spyscape.sky.org"
}

output "s3" {
  value = "${aws_s3_bucket.assets[*].id}"
}


/*

Created and Switched to workspace "production"
$ terraform workspace new production

*/
