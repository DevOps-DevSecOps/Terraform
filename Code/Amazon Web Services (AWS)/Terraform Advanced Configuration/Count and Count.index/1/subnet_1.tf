resource "aws_subnet" "public-subnet" {
  count = 2
  vpc_id     = var.vpcID
  cidr_block = var.public_subnet_cidr[count.index]
  availability_zone = var.az[count.index]
}
