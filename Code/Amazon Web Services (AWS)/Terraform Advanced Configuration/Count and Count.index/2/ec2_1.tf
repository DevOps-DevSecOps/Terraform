provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "my-machine" {
  count = 4 
   
  ami = "ami-0742a572c2ce45ebf"
  instance_type = "t2.micro"
  tags = {
    Name = "my-machine-${count.index}"
         }
}
