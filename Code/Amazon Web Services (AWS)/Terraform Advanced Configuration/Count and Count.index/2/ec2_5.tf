resource "aws_instance" "demo" {
  count         = 3
  ami           = "ami-06195c6348fb0b567"
  instance_type = "t2.micro"

  tags = {
    name = "Demo System"
  }
}

output "instance_id-0" {
  value = aws_instance.demo[0].id
}

output "instance_id-1" {
  value = aws_instance.demo.1.id
}

output "instance_id-ALL" {
  value = aws_instance.demo.*.id
}

output "instance_id" {
  value = aws_instance.demo[*].id
}
