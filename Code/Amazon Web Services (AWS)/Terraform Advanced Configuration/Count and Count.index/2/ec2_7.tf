# In this example, three EC2 instances will be created with the same configuration.

resource "aws_instance” “example" {
  count = 3
  ami = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"
}
