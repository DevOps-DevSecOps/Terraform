provider "aws" {
  region = "us-east-1"
}

resource "aws_iam_user" "users" {
  count = length(var.user_name)
  name = var.user_name[count.index]
}
 
variable "user_name" {
  type = list(string)
  default = ["user1","user2","user3","user4"]
}
