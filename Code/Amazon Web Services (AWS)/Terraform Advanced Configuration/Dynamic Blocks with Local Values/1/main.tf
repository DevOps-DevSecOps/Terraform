locals {
  ingress_rules = [
      {
          description = "SSH from system"
          port = "22" 
      },
      {
          description = "HTTP from system"
          port = "80"
      },
      {
          description = "HTTPS from system"
          port = "443"
      }
  ]
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh_http_https"
  description = "Allow SSH HTTP HTTPS inbound traffic"
  vpc_id      = "vpc-0720a5c2fc7a18218"

  dynamic "ingress" {
    for_each = local.ingress_rules
    content{
        description      = ingress.value.description
        from_port        = ingress.value.port
        to_port          = ingress.value.port
        protocol         = "tcp"
        cidr_blocks      =  ["${chomp(data.http.myip.body)}/32"]
    }
  }
}
