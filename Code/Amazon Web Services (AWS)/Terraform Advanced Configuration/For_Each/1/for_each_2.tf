provider "aws" {
  profile = "aws"
  region = "us-east-1"
}

variable "vpc_cidr" {
  default = {
    "dev"  =  "10.0.0.0/16", 
    "prod" =  "10.0.0.0/24"
  }
}

resource "aws_vpc" "main" {
  for_each    =  var.vpc_cidr
  cidr_block  =  each.value

  tags = {
    "Name" = each.key
  }
}
