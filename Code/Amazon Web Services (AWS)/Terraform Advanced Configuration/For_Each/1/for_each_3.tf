provider "aws" {
  region = "us-east-1"
}

variable "vpcs" {
  default = [
    "10.0.0.0/16", 
    "10.1.0.0/16"
  ]
}

resource "aws_vpc" "main" {
  for_each = toset(var.vpcs)
  cidr_block  =  each.value
}
