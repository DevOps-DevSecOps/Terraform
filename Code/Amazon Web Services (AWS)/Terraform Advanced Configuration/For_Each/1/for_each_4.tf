provider "aws" {
  region = "us-east-1"
}

variable "vpcs" {
  default = {
    app-one  =  "10.0.0.0/16", 
    app-two  =  "10.1.0.0/16"
  }
}

resource "aws_vpc" "main" {
  for_each    =  var.vpcs
  cidr_block  =  each.value

  tags = {
    "Name" = each.key
  }
}
