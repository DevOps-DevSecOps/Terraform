provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "my-machine" {
  ami = "ami-06195c6348fb0b567"
  for_each  = {
     "n.virginia" = "t2.micro"
     "n.virginia" = "t2.medium"
  }
  instance_type    = each.value 
  key_name         = each.key
  tags =  {
    Name = each.value 
  }
}
 
resource "aws_iam_user" "accounts" {
  for_each = toset( ["Account1", "Account2", "Account3", "Account4"] )
  name     = each.key
}
