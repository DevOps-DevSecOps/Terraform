# In this example, the var.instance_config map defines different configurations for each instance, and for_each is used to create instances based on this map.

variable "instance_config" {
  type = map(object({
    ami = string
    instance_type = string
  }))
  default = {
    example1 = { ami = "ami-0c55b159cbfafe1f0", instance_type = "t2.micro" }
    example2 = { ami = "ami-0123456789abcdef0", instance_type = "t2.small" }
    example3 = { ami = "ami-abcdef1234567890", instance_type = "t2.medium" }
  }
}

resource "aws_instance" "example" {
  for_each = var.instance_config

  ami = each.value.ami
  instance_type = each.value.instance_type
}
