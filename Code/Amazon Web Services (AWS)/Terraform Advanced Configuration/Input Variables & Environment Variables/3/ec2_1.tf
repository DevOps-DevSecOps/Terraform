provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = "us-west-2"
}

variable "aws_access_key" {}

variable "aws_secret_key" { }


resource "aws_instance" "example_instance" {
  ami           = "ami-04893cdb768d0f9ee"
  instance_type = "t2.micro"

  tags = {
    env = "PROD_SERVER"
  }

}
