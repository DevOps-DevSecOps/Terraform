provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = "us-west-2"
}

variable "aws_access_key" {}

variable "aws_secret_key" { }


resource "aws_instance" "example_instance" {
  ami           = "ami-0e21f10bcf3ea0940"
  instance_type = "t2.micro"

  tags = {
    name = "APP",
    env = "PROD_SERVER"
  }

}
