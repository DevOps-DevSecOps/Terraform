resource "aws_instance" "server_one" {
  ami           = var.ec2_image
  instance_type = var.ec2_instance_type
}

resource "aws_instance" "server_two" {
  ami           = var.ec2_image
  instance_type = var.ec2_instance_type
}
