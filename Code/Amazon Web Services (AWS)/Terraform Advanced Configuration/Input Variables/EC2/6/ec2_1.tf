terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

provider "aws" {
  region = "us-west-1"
}

variable "image_id" { }

resource "aws_instance" "example" {
  instance_type = "t2.micro"
  ami           = var.image_id
}
