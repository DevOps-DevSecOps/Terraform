variable "bucket_name" {
  type = string
  default = "mybucket"
  description = "name of s3 bucket"
}

variable "acl" {
  type = string
  default = "private"
}

variable "region" {
  default = "us-east-2"
}
