provider "aws" {
  profile = "aws"
  region  = var.region
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_bucket_name
  acl    = var.acl

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

variable "region"{
  type = string
  description = "location"
  default = "us-east-2"
}

variable "s3_bucket_name"{
  type = string
  default = "mybucket"
  description = "name of s3 bucket"
}

variable "acl"{
  type = string
  description = "private s3 bucket"
  default = "private"
}
