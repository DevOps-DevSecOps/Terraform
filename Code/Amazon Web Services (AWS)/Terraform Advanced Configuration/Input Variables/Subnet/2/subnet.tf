resource "aws_subnet" "public" {
  vpc_id     = var.vpcID
  cidr_block = "172.31.33.0/24"
  availability_zone = "us-west-1a"

  tags = {
    Name = "${var.subnet_name}-vpc"
  }
}
