provider "aws" {
  region = "us-east-1"
}

locals {
  instance_type = toset([
    "t2.micro",
    "t2.medium",
  ])
}
 
resource "aws_instance" "server" {
  for_each      = local.instance_type
 
  ami           = "ami-0a91cd140a1fc148a"
  instance_type = each.key
   
  tags = {
    Name = "Ubuntu-${each.key}"
  }
}
