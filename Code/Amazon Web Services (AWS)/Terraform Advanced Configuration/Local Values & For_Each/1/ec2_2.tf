provider "aws" {
  region = "us-east-1"
}

locals {
  instance_type = {
    key1 = "t2.micro",
    key2 = "t2.medium",
  }
}
 
resource "aws_instance" "server" {
  for_each      = local.instance_type
 
  ami           = "ami-0a91cd140a1fc148a"
  instance_type = each.value
   
  tags = {
    Name = "Ubuntu-${each.key}"
  }
}
