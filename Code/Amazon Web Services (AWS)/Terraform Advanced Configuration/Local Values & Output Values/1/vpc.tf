locals {
  vpc_tags = {
    Name = "AWS VPC"
    Project = "Terraform-AWS"
    Environment = "DEV"  
  }
}

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = local.vpc_tags
}

output "vpcID" {
  value = aws_vpc.main.id
}
