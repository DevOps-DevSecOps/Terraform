variable "common_tags" {
  default = {
    Project = "Terraform-AWS"
    Environment = "DEV"
  }
}
