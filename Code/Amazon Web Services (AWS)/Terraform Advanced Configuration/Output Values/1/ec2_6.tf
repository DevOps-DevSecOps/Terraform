provider "aws" {
  profile = "default"
  region  = var.region_name
}

resource "aws_instance" "monitor_server" {
  instance_type = var.ec2_instance_type
  ami           = var.monitor_server_ami
}

resource "aws_instance" "app_server" {
  instance_type = var.ec2_instance_type
  ami           = var.app_server_ami
}

variable "region_name" {     
  type = string
  default = "us-east-1"
}

variable "ec2_instance_type" {     
  type = string
  default = "t2.micro"
}

variable "monitor_server_ami" {     
  type = string
  default = "ami-04893cdb768d0f9ee"
}

variable "app_server_ami" {     
  type = string
  default = "ami-04893cdb768d0f9ee"
}

output "app_server_public_ip" {
  value = aws_instance.app_server.public_ip
}

output "monitor_server_public_ip" {
  value = aws_instance.monitor_server.public_ip
}
