provider "aws" {
  region = "us-east-1"
  profile = "dev"
}
variable "prefix" {
  description = "servername prefix"
  default = "gritfyapp"
}
resource "aws_instance" "web" {
  ami           = "ami-007a18d38016a0f4e"
  instance_type = "t3.medium"
  count = 1
  vpc_security_group_ids = [
    "sg-0d8bdc71aee9f"
  ]
  user_data = "${file("init.sh")}"
  subnet_id = "subnet-00514b9f4cd6d4"
  tags = {
    Name = "${var.prefix}${count.index}"
  }
}
output "instances" {
  value       = "${aws_instance.web.*.private_ip}"
  description = "PrivateIP address details"
}
