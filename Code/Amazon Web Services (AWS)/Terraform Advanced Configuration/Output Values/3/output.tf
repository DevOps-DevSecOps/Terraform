output "name_of_bucket" {
  value = aws_s3_bucket.bucket.bucket
}

output "bucket_ID" {
  value = aws_s3_bucket.bucket.id
}

output "arn" {
  value = aws_s3_bucket.bucket.arn
}

output "tags" {
  value = aws_s3_bucket.bucket.tags
}

output "tags_all" {
  value = aws_s3_bucket.bucket.tags_all
}

output "region" {
  value = aws_s3_bucket.bucket.region
}
