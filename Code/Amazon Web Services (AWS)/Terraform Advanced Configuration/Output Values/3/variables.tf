variable "region"{
  type = string
  description = "location"
  default = "us-east-2"
}

variable "s3_bucket_name"{
  type = string
  default = "mybucket"
  description = "name of s3 bucket"
}

variable "acl"{
  type = string
  description = "private s3 bucket"
  default = "private"
}
