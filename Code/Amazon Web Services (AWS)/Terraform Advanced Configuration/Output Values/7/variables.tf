variable "region"{
  type = string
  description = "location"
  default = "us-east-1"
}

variable "Name" {}

variable "Environment" {
}

variable "s3_bucket_name"{
  type = string
  description = "name of s3 bucket"
}
