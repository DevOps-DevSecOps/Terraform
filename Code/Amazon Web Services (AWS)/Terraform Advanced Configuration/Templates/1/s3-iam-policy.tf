data "template_file" "mydata"{
    template = "${file("s3-iam-policy.json")}"
    vars = {
        bucket_name = "javahome-dev"
    }
}

output "name" {
  value = "${data.template_file.mydata.rendered}"
}
