provider "aws" {
  region = "us-east-1"
}

variable "users" {
  type = list(object({
    name = string
    location = string
  }))
  default = [ { 
    location = "india"
    name = "hari"  
  },
  {
    location = "usa"
    name = "john"  
  },
  {
    location = "uk"
    name = "michel"      
  } ]
}

output "names" {
  value = var.users[*].name
}

output "locations" {
  value = var.users[*].location
}
