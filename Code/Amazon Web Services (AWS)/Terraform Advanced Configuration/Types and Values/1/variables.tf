variable "cidr"{
  default = "10.0.0.0/16"
  type = string
}

variable "name"{
  default = "AWS VPC Terraform"
  type = string
}

variable "environment"{
  default = "DEV"
  type = string
}

variable "empID" {
  default = "123456"
  type = number
}

variable "dns" {
  default = true
  type = bool
}

variable "hostnames" {
  default = true
  type = bool
}
