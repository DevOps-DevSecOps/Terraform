resource "aws_instance" "instance-1" {
  ami = "ami-075826f0c65a2c4b1"                                                   # existing infrastructure resource
  tags = {                                                                        # existing infrastructure resource
    "Name" = "aws-cloud9-ubuntu-6fbb59f5307e4b9a981cb8b295df5a1d"
  }
  instance_type = "t2.micro"                                                      # existing infrastructure resource
}
