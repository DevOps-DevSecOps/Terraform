# version
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.8.0"
    }
  }
}

# provider
provider "aws" {
  region = "us-west-2"
}

# variables
variable "count_no" {
  type = number
}

variable "ami_id" {}

# EC2
resource "aws_instance" "instance" {
  ami = var.ami_id                                                                # existing infrastructure resource
  instance_type = "t2.micro"                                                      # existing infrastructure resource
  tags = {                                                                        # existing infrastructure resource
    Name = "RedHat"
  }
  count = var.count_no
}

