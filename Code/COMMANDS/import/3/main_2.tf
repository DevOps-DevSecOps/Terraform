provider "aws" {
    region = "us-west-2"
    profile = "terraform-user"
}

resource "aws_instance" "demo-instance" {
  ami           = "ami-095413544ce52437d"
  instance_type = "unknown"
}
