resource "aws_instance" "web" {
  ami           = "ami-0638cf52a5b22be19"
  instance_type = "t2.micro"
}

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "my-tf-test-bucket-123987546"
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

