provider "aws" {
  region = "us-east-1"
}

variable "ami" {
  default = "ami-05cc83e573412838f"
}

variable "instance_type" {
  type = string
  default = "t3.medium"
}

resource "aws_instance" "my_vm" {
 count         = 3
 ami           = var.ami
 instance_type = var.instance_type


 tags = {
   Name = "Server ${count.index}"
 }
}
