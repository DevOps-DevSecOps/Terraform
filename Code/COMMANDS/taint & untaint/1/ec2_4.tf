provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "ubuntu" {
 most_recent = true


 filter {
   name   = "name"
   values = ["ubuntu*"]
 }
}

locals {
 instances = {
   instance1 = {
     ami           = data.aws_ami.ubuntu.id
     instance_type = "t3.micro"
   }
   instance2 = {
     ami           = data.aws_ami.ubuntu.id
     instance_type = "t3.micro"
   }
   instance3 = {
     ami           = data.aws_ami.ubuntu.id
     instance_type = "t3.medium"
   }
 }
}


resource "aws_instance" "my_vm" {
 for_each      = local.instances
 ami           = each.value.ami
 instance_type = each.value.instance_type


 tags = {
   Name = format("VM_%s", each.key)
 }
}
