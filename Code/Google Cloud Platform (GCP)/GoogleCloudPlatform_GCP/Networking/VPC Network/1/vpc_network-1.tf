terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "6.14.1"
    }
  }
}


provider "google" {
  project     = "smart-portfolio-441916-n8"
  region      = "us-central1"
}


resource "google_compute_network" "VPC" {
  name = "vpc"
}


resource "google_compute_subnetwork" "SUBNET" {
  name          = "subnet"
  ip_cidr_range = "10.2.0.0/16"
  region        = "us-central1"
  network       = google_compute_network.VPC.name
  secondary_ip_range {
    range_name    = "secondary-range"
    ip_cidr_range = "192.168.10.0/24"
  }
}


resource "google_compute_firewall" "FIREWALL" {
  name    = "firewall"
  network = google_compute_network.VPC.name
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }
  source_tags = ["web"]
}
