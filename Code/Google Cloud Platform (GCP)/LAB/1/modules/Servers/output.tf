output "name_of_server"{
    value = google_compute_instance.VM.name
}

output "id_of_server"{
    value = google_compute_instance.VM.id
}

output "instance_id_of_server"{
    value = google_compute_instance.VM.instance_id
}

output "ip_of_server"{
    value = google_compute_instance.VM.network_interface
}
