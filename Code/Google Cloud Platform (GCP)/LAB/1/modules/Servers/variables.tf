variable "server_name" {
  description = "Name of the server"
}

variable "machine_type" {
  description = "Name of the machine type"
  default = "n2-standard-2"
}

variable "zone_name" {
  description = "Name of the zone"
}

variable "network_name" {
  description = "Name of the network"
}

variable "subnet_name" {
  description = "Name of the subnet"
}
