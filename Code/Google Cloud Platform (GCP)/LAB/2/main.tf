provider "google" {
  credentials = "./terraform-sa.json"
  project     = var.Project_ID
}

module "network" {
  source = "./modules/Network/"
  project_name = var.Project_ID
  vpc_name = "test-dev-vpc"
  subnet1_name = ["subnet1-a","subnet1-b","subnet1-c"]
  subnet1_cidr = ["10.100.1.0/24", "10.100.2.0/24", "10.100.3.0/24"]
  subnet2 = var.subnet_2
  subnet3 = var.subnet_3
  source_ranges = ["0.0.0.0/0"]
}

module "server" {
  source        = "./modules/Servers"
  server_name   = "devops"
  network_name  = module.network.id_of_VPC
  subnet_name   = module.network.name_of_SUBNET-1_c
}
