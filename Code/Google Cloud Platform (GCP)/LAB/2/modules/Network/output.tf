### VPC ###
output "name_of_VPC" {
    value = google_compute_network.VPC.name
}

output "id_of_VPC" {
    value = google_compute_network.VPC.id
}


### SUBNET-1 ###
output "name_of_SUBNET-1_a" {
    value = google_compute_subnetwork.SUBNET-1[0].name
}

output "id_of_SUBNET-1_a" {
    value = google_compute_subnetwork.SUBNET-1[0].id
}

output "name_of_SUBNET-1_b" {
    value = google_compute_subnetwork.SUBNET-1[1].name
}

output "id_of_SUBNET-1_b" {
    value = google_compute_subnetwork.SUBNET-1[1].id
}

output "name_of_SUBNET-1_c" {
    value = google_compute_subnetwork.SUBNET-1[2].name
}

output "id_of_SUBNET-1_c" {
    value = google_compute_subnetwork.SUBNET-1[2].id
}


### SUBNET-2 ###
output "name_of_SUBNET-2_a_b_c" {
  value = { for s2, subnet2 in google_compute_subnetwork.SUBNET-2 : s2 => subnet2.name }
}

output "id_of_SUBNET-2_a_b_c" {
  value = { for s2, subnet2 in google_compute_subnetwork.SUBNET-2 : s2 => subnet2.id }
}

output "cidr_range_of_SUBNET-2_a_b_c" {
  value = { for s2, subnet2 in google_compute_subnetwork.SUBNET-2 : s2 => subnet2.ip_cidr_range }
}


### SUBNET-3 ###
output "SUBNET-3_a_b_c" {
  value = { for s3, subnet3 in google_compute_subnetwork.SUBNET-3 : s3 => {
    name       = subnet3.name
    id         = subnet3.id
    cidr_range = subnet3.ip_cidr_range
  }}   
}


### Firewall ###
output "FIREWALL" {
  value = google_compute_firewall.FIREWALL
}
