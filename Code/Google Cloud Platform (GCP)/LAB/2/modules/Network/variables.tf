variable "project_name" { }

variable "vpc_name" {
    description = "Name of the VPC"
}

variable "subnet1_name" {
  description = "Name of the SUBNET-1"
}

variable "subnet1_cidr" {
  description = "CIDR range for SUBNET-1"
}

variable "subnet2" {
  description = "A map of subnet names and CIDR blocks."
  type        = map(string)
}

variable "subnet3" {
  description = "A map of subnet names and CIDR blocks."
  type = map(object({
    name = string
    ip_cidr_range = string
  }))
}

variable "source_ranges" {
  description = "IP addresses that are allowed to initiate traffic towards a protected network"
}
