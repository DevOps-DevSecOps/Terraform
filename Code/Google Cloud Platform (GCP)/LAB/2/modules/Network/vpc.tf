### VPC ###
resource "google_compute_network" "VPC" {
  project                 = var.project_name
  name                    = var.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1460
}


### Subnet ###
resource "google_compute_subnetwork" "SUBNET-1" {
  count         = 3 
  name          = var.subnet1_name[count.index]
  ip_cidr_range = var.subnet1_cidr[count.index]
  region        = "us-central1"
  network       = google_compute_network.VPC.name
}

resource "google_compute_subnetwork" "SUBNET-2" {
  for_each      = var.subnet2
  name          = each.key
  ip_cidr_range = each.value
  region        = "us-central1"
  network       = google_compute_network.VPC.id
}

resource "google_compute_subnetwork" "SUBNET-3" {
  for_each      = var.subnet3
  name          = each.value.name
  ip_cidr_range = each.value.ip_cidr_range
  region        = "us-central1"
  network       = google_compute_network.VPC.self_link
}


### Firewall ###
resource "google_compute_firewall" "FIREWALL" {
  name    = "firewall"
  network = google_compute_network.VPC.numeric_id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "3389"]
  }

  source_ranges = var.source_ranges
}
