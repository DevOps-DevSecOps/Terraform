### SUBNET-1 ###
output "GCP_SUBNET-1_A_B_C" {
    value = {
      name-1_A = module.network.name_of_SUBNET-1_a
      id-1_A   = module.network.id_of_SUBNET-1_a
      name-1_B = module.network.name_of_SUBNET-1_b
      id-1_B   = module.network.id_of_SUBNET-1_b
      name-1_C = module.network.name_of_SUBNET-1_c
      id-1_C   = module.network.id_of_SUBNET-1_c
    }
}


### SUBNET-2 ###
output "GCP_SUBNET-2_A" {
    value = {
      name = module.network.name_of_SUBNET-2_a_b_c
      id   = module.network.id_of_SUBNET-2_a_b_c
      cidr_range = module.network.cidr_range_of_SUBNET-2_a_b_c
    }
}


### SUBNET-3 ###
output "GCP_SUBNET-3_A_B_C" {
    value = module.network.SUBNET-3_a_b_c
}


### Firewall ###
output "GCP_FIREWALL" {
  value = module.network.FIREWALL
}
