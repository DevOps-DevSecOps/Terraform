variable "subnets" {
  description = "A map of subnet configurations"
  type = map(object({
    name       = string
    region     = string
    ip_range   = string
  }))
  default = {
    "subnet-a" = {
      name     = "subnet-a"
      region   = "us-central1"
      ip_range = "10.0.1.0/24"
    },
    "subnet-b" = {
      name     = "subnet-b"
      region   = "us-east1"
      ip_range = "10.0.2.0/24"
    },
    "subnet-c" = {
      name     = "subnet-c"
      region   = "us-west1"
      ip_range = "10.0.3.0/24"
    }
  }
}
