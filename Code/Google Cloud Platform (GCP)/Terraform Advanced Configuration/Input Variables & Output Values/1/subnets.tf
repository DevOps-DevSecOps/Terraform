resource "google_compute_subnetwork" "SUBNET_1" {
  name          = "subnet-1"
  ip_cidr_range = "10.0.0.0/24"
  region        = var.Region
  network       = google_compute_network.VPC.id
}


resource "google_compute_subnetwork" "SUBNET_2" {
  name          = "subnet-2"
  ip_cidr_range = var.Subnet2_CIDR
  region        = var.Region
  network       = google_compute_network.VPC.id
}


resource "google_compute_subnetwork" "SUBNET_3" {
  name          = "subnet-3"
  ip_cidr_range = var.Subnet3_CIDR
  region        = var.Region
  network       = google_compute_network.VPC.id
}
