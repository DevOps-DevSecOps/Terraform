resource "google_compute_instance" "VM_1" {
  name         = "vm-1"
  machine_type = "e2-micro"
  zone         = "us-central1-a"

  tags = ["servers"]

  boot_disk {
    initialize_params {
      image = "ubuntu-minimal-2210-kinetic-amd64-v20230126"
    }
  }

  network_interface {
    network = google_compute_network.VPC.id

    subnetwork = google_compute_subnetwork.SUBNET_2.id

    access_config {
      // Ephemeral public IP
    }
  }

  metadata = {
    ssh-keys = "YOUR_USERNAME:${file("./ssh.pub")}"
  }

  metadata_startup_script = "echo hi > /test.txt"

}
