provider "google" {
  project     = "smart-portfolio-441916-n8"
  region      = "us-central1"
}


variable "name" {
  description = "name for all below resources"
  type = string
  default = "gcp"
}


resource "google_compute_network" "VPC" {
  name = "vpc-${var.name}"
}


resource "google_compute_subnetwork" "SUBNET" {
  name          = "subnet-${var.name}"
  ip_cidr_range = "10.2.0.0/16"
  region        = "us-central1"
  network       = google_compute_network.VPC.name
  secondary_ip_range {
    range_name    = "secondary-range"
    ip_cidr_range = "192.168.10.0/24"
  }
}


resource "google_compute_firewall" "FIREWALL" {
  name    = "${var.name}-firewall"
  network = google_compute_network.VPC.name
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }
  source_ranges = ["0.0.0.0/0"]
}
