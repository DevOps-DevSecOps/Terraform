provider "google" {
  project = "smart-portfolio-441916-n8"
  region  = "us-central1"  
}


resource "google_compute_network" "vpc" {
  name                    = "my-vpc-network"
  auto_create_subnetworks = false
}


locals {
  subnets = {
    "subnet-a" = {
      ip_cidr_range = "10.0.1.0/24"
      region        = "us-central1"
    }
    "subnet-b" = {
      ip_cidr_range = "10.0.2.0/24"
      region        = "us-central1"
    }
    "subnet-c" = {
      ip_cidr_range = "10.0.3.0/24"
      region        = "us-central1"
    }
  }
}


resource "google_compute_subnetwork" "subnet" {
  for_each      = local.subnets
  name          = each.key
  ip_cidr_range = each.value.ip_cidr_range
  region        = each.value.region
  network       = google_compute_network.vpc.id
}
