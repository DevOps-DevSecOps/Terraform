By default, Terraform stores state locally in a file named "terraform.tfstate".
This default configuration can make Terraform usage difficult for teams when multiple users run Terraform at the same time and each machine has its own understanding of the current infrastructure.

[terraform init]
When you run terraform init for the first time, the Cloud Storage bucket that you specified in the FILE_NAME.tf file doesn't exist yet. 
So Terraform initializes a local backend to store state in the local file system.

[terraform apply]
When prompted, enter yes.
When you run terraform apply for the first time, Terraform provisions the Cloud Storage bucket for storing the state. 
It also creates a local file; the contents of this file instruct Terraform to use the Cloud Storage bucket as the remote backend to store state.

[terraform init -migrate-state]
When prompted, enter yes.
Terraform detects that you already have a state file locally and prompts you to migrate the state to the new Cloud Storage bucket.
After running this command, your Terraform state is stored in the Cloud Storage bucket.
Terraform pulls the latest state from this bucket before running a command, and pushes the latest state to the bucket after running a command.

[terraform destroy]



# LINK :- https://cloud.google.com/docs/terraform/resource-management/store-state
