terraform

terraform version
terraform -version
terraform --version

terraform init
terraform init -upgrade                                                                 # upgrade/downgrade the provider plugin version
terraform init -reconfigure                                                             # any changes happened in the code after that execute it
terraform init -migrate-state

terraform fmt                                                                           # adjusting spaces indent, fix the syntax error
terraform fmt -diff                                                                     # display diffs of formatting changes
terraform fmt -check                                                                    # check if the input is formatted, exit status will be 0 if all input is properly formatted, If not exit status will be non-zero are not properly formatted

terraform validate

terraform plan
terraform plan -out FILE_NAME                                                           # saving a plan output in a file
terraform plan -out=FILE_NAME
terraform plan -out="FILE_NAME"
terraform plan -lock=false
terraform plan -refresh-only
terraform plan -var NAME=VALUE
terraform plan -var NAME="VALUE"
terraform plan -var 'NAME=VALUE'                                                        # passing values to variables from the CLI
terraform plan -var "NAME=VALUE"
terraform plan -var="NAME=VALUE"
terraform plan -var='NAME=VALUE' -var 'NAME=VALUE' --var-file=./FOLDER_NAME/FILE_NAME.tfvars
terraform plan -var-file FILE_NAME.tfvars
terraform plan -var-file 'FILE_NAME.tfvars'
terraform plan -var-file "FILE_NAME.tfvars"
terraform plan -var-file=FILE_NAME.tfvars
terraform plan -var-file='FILE_NAME.tfvars'
terraform plan -var-file="FILE_NAME.tfvars"
terraform plan --var-file="FILE_NAME.tfvars"
terraform plan --var-file="FOLDER_NAME/FILE_NAME.tfvars"
terraform plan --var-file=./FOLDER_NAME/FILE_NAME.tfvars
terraform plan --var-file="./FOLDER_NAME/FILE_NAME.tfvars"
terraform plan -replace=RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform plan -replace="RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"
terraform plan -replace=module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform plan -replace="module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"
terraform plan --replace=RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform plan --replace='RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME'
terraform plan --replace=module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform plan --replace='module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME'

terraform apply
terraform apply FILE_NAME                                                               # executing apply command with the plan output file
terraform apply "FILE_NAME"                                                             # executing apply command with the plan output file
terraform apply -auto-approve
terraform apply -auto-approve FILE_NAME                                                 # executing apply command with the plan output file with auto approval
terraform apply --auto-approve
terraform apply -lock=false                                                             # don't hold a state lock during the operation
terraform apply -refresh-only
terraform apply -var NAME=VALUE
terraform apply -var NAME="VALUE"
terraform apply -var 'NAME=VALUE'
terraform apply -var 'NAME=VALUE' -var 'NAME=VALUE'
terraform apply -var "NAME=VALUE"
terraform apply -var="NAME=VALUE"
terraform apply -var="NAME=VALUE" -var="NAME=VALUE"                                     # passing values to variables from the CLI
terraform apply -var='NAME=VALUE' -var 'NAME=VALUE' --var-file=./FOLDER_NAME/FILE_NAME.tfvars --auto-approve
terraform apply -var-file FILE_NAME.tfvars
terraform apply -var-file 'FILE_NAME.tfvars'
terraform apply -var-file "FILE_NAME.tfvars"
terraform apply -var-file=FILE_NAME.tfvars
terraform apply -var-file='FILE_NAME.tfvars'
terraform apply -var-file="FILE_NAME.tfvars"
terraform apply --var-file="FILE_NAME.tfvars"
terraform apply --var-file="FOLDER_NAME/FILE_NAME.tfvars"
terraform apply --var-file=./FOLDER_NAME/FILE_NAME.tfvars --auto-approve
terraform apply --var-file="./FOLDER_NAME/FILE_NAME.tfvars"
terraform apply -replace=RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform apply -replace="RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"
terraform apply -replace=module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform apply -replace="module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"
terraform apply --replace=RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform apply --replace="RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"
terraform apply --replace=module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME
terraform apply --replace="module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME"

terraform output
terraform output -json
terraform output OUTPUT_NAME
terraform output -raw OUTPUT_NAME

terraform get
terraform get -update

terraform import RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME ID
terraform import RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME NAME
terraform import module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME ID
terraform import module.MODULE_ALIAS_NAME.RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME NAME

terraform refresh                                                                       # it update the state file

terraform show                                                                          # human-readable output from a state file OR plan file
terraform show FILE_NAME                                                                # read the plan output file
terraform show -json FILE_NAME                                                          # read the plan output file in json format

terraform state list                                                                    # list all the created resources
terraform state pull                                                                    # manually download and output the state from remote state, it works with local state
terraform state push                                                                    # manually upload a local state file to remote state OR update the remote state from local, to manually update a local state file to a remote state
terraform state show                                                                    # show details about all created resources
terraform state show RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME

terraform taint RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME                                  #
terraform untaint RESOURCE_TYPE_NAME.RESOURCE_ALIAS_NAME                                #

terraform workspace list                                                                # list all existing workspaces, current workspace is indicated using an asterisk (*) marker
terraform workspace show                                                                # it will display the current workspace
terraform workspace new WORKSPACE_NAME                                                  # Created and Switched to workspace "NAME"
terraform workspace select WORKSPACE_NAME                                               # Switched to workspace "NAME", the named workspace must already exist
terraform workspace delete WORKSPACE_NAME                                               # to delete a workspace, it must already exist, it must not be tracking resources, and it must not be your current workspace

terraform force-unlock LOCK_ID                                                          # manually unlock the state, removes the lock on the state for the current configuration, lock is dependent on the backend (aws s3 with dynamodb for state locking)

terraform login                                                                         # login into Terraform Cloud from command line
terraform logout                                                                        # logout into Terraform Cloud from command line

terraform destroy
terraform destroy -auto-approve
terraform destroy --auto-approve
terraform destroy -var NAME=VALUE
terraform destroy -var NAME="VALUE"
terraform destroy -var 'NAME=VALUE'
terraform destroy -var-file="FILE_NAME.tfvars"

[Input Variables using Environment Variables from CLI]
export TF_VAR_NAME=VALUE
export TF_VAR_NAME=VALUE TF_VAR_NAME=VALUE
export TF_VAR_NAME='VALUE'
export TF_VAR_NAME="VALUE"
